#!/usr/bin/env bash

# Check if the script is being run as root
if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root."
    exit 1
fi

# Check if running on Ubuntu by checking for the apt command
if ! command -v apt &> /dev/null
then
    echo "This script must be run on an Ubuntu OS."
    exit 1
fi

# Check if running on Proxmox by checking for the pveversion command
if command -v pveversion &> /dev/null
then
    echo "This script cannot be run on a Proxmox host."
    exit 1
fi

# Define the init.d script content
INITD_SCRIPT_NAME=sysctl
INITD_SCRIPT_CONTENT='#!/bin/sh
### BEGIN INIT INFO
# Provides:          sysctl
# Required-Start:    $local_fs
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Run sysctl command at boot
# Description:       This script runs the sysctl command at system boot
# Author:            John Anthony Mariquit <john@mariquit.com>
### END INIT INFO

# The command you want to run
COMMAND="/usr/sbin/sysctl -p"

case "$1" in
    start|restart)
        echo "Running sysctl command..."
        $COMMAND
        ;;
    stop)
        echo "Nothing to do here..."
        ;;
    *)
        echo "Usage: /etc/init.d/sysctl {start|stop|restart}"
        exit 1
        ;;
esac

exit 0
'

# Create the /etc/init.d/$INITD_SCRIPT_NAME file
echo "Creating /etc/init.d/$INITD_SCRIPT_NAME..."
echo "$INITD_SCRIPT_CONTENT" > /etc/init.d/$INITD_SCRIPT_NAME 

# Make the script executable
echo "Making the script executable..."
sudo chmod +x /etc/init.d/$INITD_SCRIPT_NAME

# Register the script with update-rc.d
echo "Registering the script with update-rc.d..."
update-rc.d $INITD_SCRIPT_NAME defaults

echo "Script installation complete. The command will run at startup."

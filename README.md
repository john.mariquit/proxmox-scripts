# proxmox-scripts



## Description

Just some scripts that help me configure my Proxmox homelab (mostly missing stuff that I can't get via [helper-scripts.com](https://helper-scripts.com/))

## What is here

> **Disable IPV6 fix for Ubuntu LXC** 

LXC containers running on Ubuntu (tested on 22.04) as of Proxmox 8.2.2 ignores the /etc/sysctl.conf.  

This fix assumes you already have the required `net.ipv6.conf.all.disable_ipv6 = 1` line on your sysctl.conf file.  

Run this command inside the container:
```
bash -c "$(wget -qLO - https://gitlab.com/john.mariquit/proxmox-scripts/-/raw/main/install_initd_sysctl.sh)"
```

> **Display IP address on login**

Nothing fancy.  Just grabs the inet line from the `ip` command.  

Provides no other functionality other than knowing what is the current IP address of the container or a VM.  

Tested on:
- [x] Alpine 3.1.9
- [x] Ubuntu 22.04

Run this command inside the container or VM:
```
echo "ip a | grep inet || true" >> .profile
``` 